/*
1 2 3
4 5 6
7 8 9
*/
#include<stdio.h>
int main(){
	int row,col,i,j;
	
	printf("Enter row size and column size");
	scanf("%d%d",&row,&col);
	
	int x[row][col];
	
	printf("Enter array element\n");
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			printf("x[%d][%d]  :",i,j);
			scanf("%d",&x[i][j]);
		}
	}
	for(i=0;i<row;i++){
		for(j=0;j<col;j++){
			printf("%d  ",x[i][j]);
		}
		printf("\n");
   }
   getch();
}
