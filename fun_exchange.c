#include<stdio.h>
void exchange(int *p, int *q){
	int t;
	t=*p;
	*p=*q;
	*q=t;
	
}
int main(){
	int x=5;
	int y=6;
	printf("before swapping\n");
	printf("x=%d\ny=%d",x,y);
	exchange(&x,&y);
	printf("\nafter swapping\n");
	printf("x=%d\ny=%d",x,y);
}
