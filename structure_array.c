#include<stdio.h>

struct employee {
	int emp_no;
};

void showData(struct employee emp) {
	printf("%d\n", emp.emp_no);
}

int main() {
	struct employee e[3];
	
	e[0].emp_no = 20;
	e[1].emp_no = 25;
	e[2].emp_no = 30;
	
	int i;
	
	for(i = 0; i < 3; i++) 
		showData(e[i]);
	
	getch();
}
