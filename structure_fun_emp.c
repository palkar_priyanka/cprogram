#include<stdio.h>

struct employee {
	int emp_no;
	int emp_age;
};

void showData(struct employee emp) {
	printf("%d", emp.emp_no);
	printf("\n%d", emp.emp_age);
}

int main() {
	struct employee e;
	e.emp_no = 20;
	e.emp_age = 24;
	showData(e);
	getch();
}
