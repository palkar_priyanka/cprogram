#include<stdio.h>

int funAddition(int x, int y){
	return x+y;	
}

int funSubstraction(int x, int y){
	return x-y;
}

int funMultiplication(int x, int y){
	return x*y;
}

float funDivision(int x,int y){
	return x/y;
}

int main(){
	printf("Enter\n");
	printf("1. Addition\n");
	printf("2. Substraction\n");
	printf("3. Multiplication\n");
	printf("4. Division\n");
	
	int (*fp)(int,int);
	int num;
	scanf("%d",&num);
	
	switch(num) {
		case 1:
			fp = funAddition; 
			break;		
		case 2:
			fp = funSubstraction;
			break;
		case 3: 
			fp = funMultiplication;
			break;
		case 4:
			fp = funDivision;
			break;	
		default:
			printf("Wrong entry");
	}
	
	int z = (*fp)(12,6);
	
	printf("%d", z);
	getch();
}
